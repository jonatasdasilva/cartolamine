/** Arquivo com conexão com MongoDB. E realiza as ações mais importantes **/
const mongoose = require('mongoose');
//mongoose.connect('mongodb://localhost:27017/cartolamine', {useNewUrlParser: true});

// Este conjuntos de váriaveis deve estar em um local protegido e criptografado.
var options = {
    useMongoClient: true,
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0
}
let user = "administrador";
let pwd = "c4Rt01@Min3&$#";
let server = "localhost";
let bd = "cartolamine";
let port = 27088;

mongoose.connect('mongodb://${user}:${pwd}@${server}:${port}/${bd}?${options}');

