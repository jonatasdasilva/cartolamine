class Cliente{
    constructor (nome, idade, dataNascimento, rua, numero, cidade, pais, bairro){
        this.nome = nome;
        this.idade = idade;
        this.dataNascimento = dataNascimento;
        this.rua = rua;
        this.numeroCasa = numero;
        this.cidade = cidade;
        this.pais = pais;
        this.bairro = bairro;
    }

    let cara = new Cliente('Jônatas', 31, '11/12/1987', 'Vou lá logo', 'Salinas', 'Brasil', 'T. Neves');
}